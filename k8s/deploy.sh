#!/bin/bash
kubectl apply -f tj-configmap.yaml
kubectl apply -f tj-secret.yaml
kubectl apply -f storageclass.yaml
kubectl apply -f pvc.yaml
kubectl apply -f rabbit.yaml
kubectl apply -f postgres.yaml
kubectl apply -f adminer.yaml
kubectl apply -f api.yaml
kubectl apply -f celery.yaml
kubectl apply -f worker.yaml
kubectl apply -f ingress.yaml
kubectl apply -f ingress-adminer.yaml
