#!/bin/bash
kubectl delete -f tj-configmap.yaml
kubectl delete -f tj-secret.yaml
kubectl delete -f rabbit.yaml
kubectl delete -f postgres.yaml
kubectl delete -f adminer.yaml
kubectl delete -f api.yaml
kubectl delete -f celery.yaml
kubectl delete -f worker.yaml
kubectl delete -f ingress.yaml
