# Сервис рассылки в kubernetes

описал кратко, возможно что-то упустил, так что подразумевается знакомство с куберами.

## Как запустить проект в Minikube:

первым делам надо собрать и загрузить кастомный образ в registry миникуба:
```shell
$ cd api
$ docker build -t api:latest .
$ minikube image load api:latest
```

Далее, нужно в рабочем каталоге проекта перейти в ветку k8s, 
в каталоге k8s находятся все манифесты, их надо запустить 
в определеннном порядке:

```shell
$ kubectl apply -f tj-configmap.yaml
$ kubectl apply -f tj-secret.yaml
$ kubectl apply -f storageclass.yaml
$ kubectl apply -f pvc.yaml
$ kubectl apply -f rabbit.yaml
$ kubectl apply -f postgres.yaml
$ kubectl apply -f adminer.yaml
$ kubectl apply -f api.yaml
$ kubectl apply -f celery.yaml
$ kubectl apply -f worker.yaml
$ kubectl apply -f ingress.yaml
```

или можно просто запустить скрипт `deploy.sh`

Должно выглядеть примерно так (3 реплики worker):

```
boris@asus:~/projects/fbrq-test-job/k8s$ kubectl get all
NAME                           READY   STATUS    RESTARTS   AGE
pod/adminer-6f4ccbf9cb-tqsrl   1/1     Running   0          169m
pod/api-5f9594595-wkjxq        1/1     Running   0          169m
pod/celery-54f6866669-9xgwp    1/1     Running   0          147m
pod/postgres-b4dbdcddd-rnfb9   1/1     Running   0          169m
pod/rabbit-7748bb76d5-z4t2z    1/1     Running   0          169m
pod/worker-5c99f5655-8smrg     1/1     Running   0          4s
pod/worker-5c99f5655-qfp2f     1/1     Running   0          4s
pod/worker-5c99f5655-s5dfx     1/1     Running   0          4s

NAME                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/adminer-service   ClusterIP   10.99.41.169     <none>        8080/TCP   169m
service/api-service       ClusterIP   10.111.245.145   <none>        8000/TCP   169m
service/db                ClusterIP   10.107.203.35    <none>        5432/TCP   169m
service/kubernetes        ClusterIP   10.96.0.1        <none>        443/TCP    103d
service/rabbit            ClusterIP   10.110.98.62     <none>        5672/TCP   169m

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/adminer    1/1     1            1           169m
deployment.apps/api        1/1     1            1           169m
deployment.apps/celery     1/1     1            1           147m
deployment.apps/postgres   1/1     1            1           169m
deployment.apps/rabbit     1/1     1            1           169m
deployment.apps/worker     3/3     3            3           4s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/adminer-6f4ccbf9cb   1         1         1       169m
replicaset.apps/api-5f9594595        1         1         1       169m
replicaset.apps/celery-54f6866669    1         1         1       147m
replicaset.apps/postgres-b4dbdcddd   1         1         1       169m
replicaset.apps/rabbit-7748bb76d5    1         1         1       169m
replicaset.apps/worker-5c99f5655     3         3         3       4s
```
Persistent volume claims:
```
boris@asus:~/projects/fbrq-test-job/k8s$ kubectl get pvc
NAME           STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-postgres   Bound    pvc-6ab16289-8aa0-4bc9-9537-050d5d2b7a92   256Mi      RWO            tj-storage     175m
```

При первом запуске надо создать таблиицы в БД postgres.
Пока это делается вручную, (хотя по уму надо сделать контейнер инициализации)
Надо зайти интерактивно в под api (точное имя пода конечно будет иное)
```shell
$ kubectl exec -it api-5f9594595-rph7m -- /bin/bash
```
и выполнить скрипт:
```shell
boris@api-5f9594595-rph7m:~/code$ python create_tables.py
```
Таблицы создаются только один раз (они сохранятся в persistent storage)

Далее, надо прописать в `/etc/hosts` адрес ноды миникуба
```
192.168.49.2    tj.loc
```
и после этого открыть в браузере http://tj.loc/docs - появится swagger UI.
