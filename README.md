# Тестовое задание

Репозиторий содержит решение [тестового задания](https://www.craft.do/s/n6OVYFVUpq0o6L)

Работающий демо-проект: 

- [Административная панель](https://tj-ui.xland.ru/)
- [Бэкэнд / API](https://test-job.xland.ru/) - может быть выключен (постоянная работа стоит денег)

## Запуск проекта

Проще всего запустить проект на машине с установленными git, Docker & docker-compose.
Должны быть свободны порты 5432, 5555, 8000 и 8080.

Склонируйте репозиторий в рабочий каталог:

```sh
$ cd projects
$ git clone https://gitlab.com/boangri/fbrq-test-job.git
$ cd fbrq-test-job
$ docker-compose up -d --build
$ docker exec -it api python create_tables.py  # только при первом запуске
```

Откройте в браузере [http://localhost:8000](http://localhost:8000)

Можно масштабировать число контейнеров workers, например:
```shell
$ docker-compose up -d --build --scale worker=3
```

## Состав проекта

Проект включает следующие контейнеры:

* db - база данных PostgreSQL, том `postgress_data`;
* adminer - вспомогательный веб-сервер (порт 8080) для управления БД;
* app - сервер FastAPI (порт 8000) - реализует REST API;
* rabbit - RabbitMQ сервер;
* celery - сервер celery;
* flower - web-интерфейс(dashboard) flower на порту 5555 для мониторинга celery;
* worker - сервер воркеров.

## Как это работает

В БД хранятся сущности "рассылки" (таблица sendings), "клиенты" (таблица clients) и "собщения" (таблица messages).
На FastAPI реализован API для CRUD-операций с данными таблицами.

Сначала надо наполнить таблицу clients. Несколько тестовых записей можно создать через Swagger-интерфейс 
[http://localhost/docs](http://localhost/docs) используя метод POST.

Для запуска рассылки необходимо создать сущность "sending" (также методом POST). Нужно задать текст сообщения 
(он общий для всех получателей но в принципе можно сделать персонализацию через темплейт)
Еще задается код и тэг, по которым отселектируются получатели этой рассылки. По умолчанию рассылка запускается немедленно
и не ограничена по времени. Но можно задать дату и время начала рассылки и дедлайн.

При создании рассылки запускается таск celery, отложенный по времени до момента, указанного как начало рассылки.
Таск запускается немедленно, если время начала рассылки в прошлом. 

Таск селектирует всех клиентов, удовлетворяющим условия фильтрации по коду и тегам и для каждого клиента создает 
сообщение AMQP в очереди RabbitMQ и запись в таблице messages в БД.  Сообщение AMQP содержит только id сообщения.

Обработка (отправка) сообщений выполняется воркерами (консьюмерами AMQP) на серверах worker 
(Их при необходимости можно запустить несколько, как показывалось ранее)

Воркер получает id сообщения из очереди, обращается к серверу API и получает недостающую информацию - номер телефона и 
текст сообщения. (Используется эндпоинт /api/sms/{id})
Далее он пытается отправить сообщение, обратившись к [серверу отправки уведомлений](https://probe.fbrq.cloud/docs).
В случае успеха воркер положительно подтверждает сообщение от AMQP и оно удаляется из очереди.
Воркер через API сообщает об успешной отправке, и сообщение помечается в БД как отправленное.

Если не удается отправить сообщение, то подтверждение не посылается, и сообщение остается в очереди. 
Через некоторое время оно будет получено, возможно другим, воркером - и так до тех пор, пока не будет отправлено или
не истечет срок отправки.

Если API обнаруживает, что истек срок доставки сообщения, то в этом случае он (API) помечает сообщение в БД
статусом "просрочено" и сообщает воркеру, что сообщение не найдено. В этом случае воркер также положительно подтверждает 
сообщение AMQP, чтобы оно было удалено из очереди на отправку.

### Комментарии к API

API состоит из групп Clients, Sendings, Messages, Sms, Statistics, Auth.

Первые 3 группы реализуют CRUD-операции с соотвествующими таблицами в БД.
Эндпоинт для создания рассылки (POST /api/sendings) требует авторизации по JWT токену.
По-идее все эндпоинты должны быть защищенными, но в целях демонстрации закрыт только один эндпоинт.

Чтобы получить токен, нужно сначала зарегистрироваться (создать учетку) в эндпоинте
/api/auth/signup. Можно сразу воспользваться возращаемым токеном, или, для реалистичности, сначала 
залогиниться под созданной учеткой и получить токен.

Далее надо авторизоваться на сервере.

Следует помнить, что созданные таким образом учетки не сохраняются в БД.

Группа Sms служит для:
- получения деталей сообщения по его id (в очереди AMQP хранятся только id сообщений, подробности берутся из БД с помощью API)
- сигнализации об успешной отправке сообщения (методом PUT)


## Дополнительные задания

### 1. тестирование написанного кода

Тесты запускаются так:
```shell
$ docker-compose exec api python -m pytest
```
В целях демонстрации написаны 2 теста для контейнера api

### 2. обеспечить автоматическую сборку/тестирование с помощью GitLab CI

Настроен пайплайн для сборки и автоматического деплоя приложения Web UI. 

См. [.gitlab-ci.yml](https://gitlab.com/boangri/test-job-ui/-/blob/main/.gitlab-ci.yml)

Состояния пайплайнов можно видеть [здесь](https://gitlab.com/boangri/test-job-ui/-/pipelines)

### 3. подготовить docker-compose для запуска всех сервисов проекта одной командой
Да, сделано именно так.

### 4. написать конфигурационные файлы (deployment, ingress, …) для запуска проекта в kubernetes 
_и описать как их применить к работающему кластеру_

Манифесты kubernetes находятся в каталоге k8s, там же имеется описание процесса деплоя приложения.
См. файл [README](https://gitlab.com/boangri/fbrq-test-job/-/blob/k8s/k8s/README.md)

### 5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI

FastAPI автоматически генерирует эту страницу. 

### 6. реализовать администраторский Web UI для управления рассылками и получения статистики по отправленным сообщениям

Создана [административная панель](https://tj-ui.xland.ru/) с ограниченным функционалом (создание новых рассылок). 
Панель работает на отдельном сервере, написана на фреймворках [vue.js](https://vuejs.org/), 
[vuetify](https://vuetifyjs.com/en/) с использованием компонент [materializecss.com](https://materializecss.com/)
Дизайн полностью подходит для работы на мобильных устрйствах (responsive)

Исходный код проекта лежит в отдельном [репозитории](https://gitlab.com/boangri/test-job-ui).

### 7. обеспечить интеграцию с внешним OAuth2 сервисом авторизации для административного интерфейса.

[Административная панель](https://tj-ui.xland.ru/) интегрирована с сервисом авторизации https://auth0.com 
Для создания новой рассылке необходимо предварительно авторизоваться с учетной записью Google или auth0.com

### 8. реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email

Не хватило времени, но задача несложная - написать скрипт, запускаемый по крону раз в сутки, читающий данные статистики 
по имеющемуся API и отсылающий на SMTP сервер. Можно добавить темплейты jinja2.

### 9. удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки.Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.

Предложенная архитектура полностью удовлетворяет этим требованиям. Задания на отправку смс хранятся в очереди RabbitMQ, 
при неудачной попытке отправки воркер не подтверждает задание, и оно периодически будет отдаваться другим воркерам до тех
пор пока не будет передано или истечет дедлайн.

### 10. реализовать отдачу метрик в формате prometheus и задокументировать эндпоинты и экспортируемые метрики

У меня есть опыт настройки и использования Prometheus, Grafana и Loki. Но к сожалению не хватило времени на настройку.
Что касается создания кастомных метрик, их можно реализовать как декораторы над энпоинтами, 
используя, например, библиотеку https://github.com/prometheus/client_python

### 11. реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно задать промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не отправлять клиенту сообщение, если его локальное время не входит в указанный интервал.

### 12. обеспечить подробное логирование на всех этапах обработки запросов, чтобы при эксплуатации была возможность найти в логах всю информацию по
- id рассылки - все логи по конкретной рассылке (и запросы на api и внешние запросы на отправку конкретных сообщений)
- id сообщения - по конкретному сообщению (все запросы и ответы от внешнего сервиса, вся обработка конкретного сообщения)
- id клиента - любые операции, которые связаны с конкретным клиентом (добавление/редактирование/отправка сообщения/…)
