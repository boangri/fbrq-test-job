from database import Base
from sqlalchemy import Column, Integer, String, Text, DateTime, ForeignKey
from datetime import datetime
from sqlalchemy.orm import relationship


class Client(Base):
    """
    Сущность "клиент" имеет атрибуты:
    • уникальный id клиента
    • номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)
    • код мобильного оператора
    • тег (произвольная метка)
    • часовой пояс
    """
    __tablename__ = 'clients'
    id = Column(Integer, primary_key=True)
    phone = Column(String(30), nullable=False, unique=True)
    code = Column(Integer, default=1)
    tag = Column(Integer, default=1)
    tz = Column(String(30), default='UTC')
    children = relationship("Message")

    def __repr__(self):
        return f"<Item id={self.id} name={self.phone}>"


class Sending(Base):
    """
    Сущность "рассылка" имеет атрибуты:
    • уникальный id рассылки
    • дата и время запуска рассылки
    • текст сообщения для доставки клиенту
    • фильтр свойств клиентов, на которых должна быть произведена рассылка (код мобильного оператора, тег)
    • дата и время окончания рассылки: если по каким-то причинам не успели разослать все сообщения -
      никакие сообщения клиентам после этого времени доставляться не должны
    """
    __tablename__ = 'sendings'
    id = Column(Integer, primary_key=True)
    start_ts = Column(DateTime, default=datetime.now())
    text = Column(Text, default='Hi there!')
    code = Column(Integer, default=1)
    tag = Column(Integer, default=1)
    stop_ts = Column(DateTime, default=None)
    children = relationship("Message")

    def __repr__(self):
        return f"<Item id={self.id} name={self.phone}>"


class Tag(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)


class Message(Base):
    """
    Сущность "сообщение" имеет атрибуты:
    • уникальный id сообщения
    • дата и время создания (отправки)
    • статус отправки
    • id рассылки, в рамках которой было отправлено сообщение
    • id клиента, которому отправили
    """
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    created_ts = Column(DateTime, default=datetime.now())
    sent_ts = Column(DateTime, default=None)
    status = Column(Integer, default=0)
    sending_id = Column(Integer, ForeignKey('sendings.id'))
    client_id = Column(Integer, ForeignKey('clients.id'))
