from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy import create_engine
import os
from settings import settings


username = settings.POSTGRES_USER
password = settings.POSTGRES_PASS
host = settings.POSTGRES_HOST
port = settings.POSTGRES_PORT
database = settings.POSTGRES_DB

# engine = create_engine("postgresql://myuser:password@db/data", echo=True)
engine = create_engine(f"postgresql://{username}:{password}@{host}/{database}", echo=True)

print(">>>", f"postgresql://{username}:{password}@{host}/{database}")

Base = declarative_base()
SessionLocal = sessionmaker(bind=engine)

db = SessionLocal()

