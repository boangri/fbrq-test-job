import httpx
import json


def get_params(msg_id:int) -> httpx.Response:
    url = 'http://api-service:8000/api/sms/'
    headers = {'Accept': 'application/json'}
    with httpx.Client() as c:
        return c.get(url + str(msg_id), headers=headers)


def send_sms(msg_id: int, phone: str, text: str) -> httpx.Response:
    jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Nzg3ODEzMzksImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkJvcmlzR3JpYm92c2tpeSJ9.xo5Oo1hGaPjFu91jwoG_qu7LMSoU7bPwp4R09CxBu2A'
    url = 'https://probe.fbrq.cloud/v1/send/'
    headers = {'Authorization': 'Bearer ' + jwt, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    payload = json.dumps({
        "id": msg_id,
        "phone": phone,
        "text": text
    })
    with httpx.Client() as c:
        return c.post(url + str(msg_id), data=payload, headers=headers)


def send_fake(msg_id: int, phone: str, text: str) -> bool:
    return True


def ack_message(msg_id: int):
    """
    Вызовом этого метода сигнализируем об успешной отправке сообщения
    """
    url = 'http://api-service:8000/api/sms/'
    headers = {'Accept': 'application/json'}
    with httpx.Client() as c:
        payload = json.dumps({})
        return c.put(url + str(msg_id), headers=headers, data=payload)
