import sys
import os
import pika
from client import get_params, send_sms, ack_message, send_fake
from settings import settings

username = settings.RABBITMQ_USER
password = settings.RABBITMQ_PASS
host = settings.RABBITMQ_HOST
port = settings.RABBITMQ_PORT


def callback(ch, method, properties, body):
    """
    Получить id сообщения из AMQP, по нему получить номер телефона и текст сообщения,
    попытаться отправить СМС, в случае успеха подтвердить отправку.
    """
    msg_id = int(body.decode())
    print(" [x] Received %d" % msg_id)
    r = get_params(msg_id)
    if r.status_code != 200:
        """
        Если получен код 404 - это означает что у сообщения истек срок отправки.
        Удаляем задание из очереди AMQP.
        """
        if r.status_code == 404:
            ch.basic_ack(delivery_tag=method.delivery_tag)
        return
    data = r.json()
    text = data['text']
    phone = data['phone']
    """ Пытаемся отправить СМС через внешний сервис """
    # r = send_sms(msg_id, phone, text)
    # if r.status_code == 200:
    """ Имитация отправки """
    if send_fake(msg_id, phone, text):
        """ В случае успеха удаляем задание из очереди и сообщаем об отправке на наш API """
        ch.basic_ack(delivery_tag=method.delivery_tag)
        ack_message(msg_id)
        return


def main():
    print(username, password, host, port)
    credentials = pika.PlainCredentials(username, password)
    parameters = pika.ConnectionParameters(host, port, '/', credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='msg_queue', durable=True)
    channel.basic_consume(queue='msg_queue',
                          # auto_ack=True,
                          on_message_callback=callback)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)