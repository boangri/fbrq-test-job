import os
import models
from database import db
from celery import Celery
import pika

celery = Celery(__name__)
username = os.environ.get('RABBITMQ_USER', 'admin')
password = os.environ.get('RABBITMQ_PASS', 'mypass')
host = os.environ.get('RABBITMQ_HOST', 'rabbit')
port = os.environ.get('RABBITMQ_PORT', '5672')
celery.conf.broker_url = f"amqp://{username}:{password}@{host}:{port}"
celery.conf.result_backend = f"amqp://{username}:{password}@{host}:{port}"


@celery.task(name="create_task")
def create_task(id, code, tag):
    print(f"id={id} code={code} tag={tag}")
    clients = db.query(models.Client).all()

    credentials = pika.PlainCredentials(username, password)
    parameters = pika.ConnectionParameters(host, port, '/', credentials)
    conn = pika.BlockingConnection(parameters)

    channel = conn.channel()
    channel.queue_declare(queue='msg_queue', durable=True)

    for client in clients:
        message = models.Message(status=0, sending_id=id, client_id=client.id)
        db.add(message)
        db.commit()
        db.refresh(message)
        print(f"id={client.id} phone={client.phone} => message_id={message.id}")
        channel.basic_publish(exchange='',
                              routing_key='msg_queue',
                              body=str(message.id),
                              properties=pika.BasicProperties(
                                  delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
                              ))
        print(f" [x] Sent messageId={message.id}")
    conn.close()
    return True
