from pydantic import BaseModel, Field, EmailStr
from datetime import datetime
from typing import Optional
from email.policy import default


class Client(BaseModel):
    """
    phone = Column(String(30), nullable=False, unique=True)
    code = Column(Integer, default=1)
    tag = Column(Integer, default=1)
    tz = Column(String(30), default='UTC')
    """
    phone: str
    code: int
    tag: int
    tz: str

    class Config:
        orm_mode = True


class Sending(BaseModel):
    """
    start_ts = Column(DateTime, default=datetime.utcnow())
    text = Column(Text, 'Hi there!')
    code = Column(Integer, default=1)
    tag = Column(Integer, default=1)
    stop_ts = Column(DateTime, default=None)
    """
    start_ts: datetime   # .utcnow()
    text: str
    code: int
    tag: int
    stop_ts: Optional[datetime] = None

    class Config:
        orm_mode = True


class Message(BaseModel):
    """
    created_ts = Column(DateTime, default=datetime.utcnow())
    sent_ts = Column(DateTime, default=None)
    status = Column(Integer)
    sending_id = Column(Integer, ForeignKey('sendings.id'))
    client_id = Column(Integer, ForeignKey('clients.id'))
    """
    created_ts: datetime
    sent_ts: Optional[datetime] = None
    status: int = 0
    sending_id: int
    client_id: int

    class Config:
        orm_mode = True


class Sms(BaseModel):
    phone: str
    text: str


class Stat(BaseModel):
    """
    Формат статистики по рассылкам
    """
    sendings_total: int = 0
    sms_total: int = 0
    status_pending: int = 0
    status_sent: int = 0
    status_expired: int = 0


class UserSchema(BaseModel):
    fullname: str = Field(default=None)
    email: EmailStr = Field(default=None)
    password: str = Field(default=None)
    class Config:
        the_schema_= {
            "user_demo": {
                "fullname": "Boris Gribovskiy",
                "email": "xinu@yandex.ru",
                "password": "12345",
            }
        }


class UserLoginSchema(BaseModel):
    email: EmailStr = Field(default=None)
    password: str = Field(default=None)
    class Config:
        the_schema_= {
            "user_demo": {
                "email": "xinu@yandex.ru",
                "password": "12345",
            }
        }
