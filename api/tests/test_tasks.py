def test_home(test_app):
    response = test_app.get("/")
    assert response.status_code == 200


def test_get_clients(test_app):
    response = test_app.get("/api/clients")
    assert response.status_code == 200
