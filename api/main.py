from fastapi import FastAPI, Request
from routers.clients import router as clients_router
from routers.sendings import router as sendings_router
from routers.messages import router as messages_router
from routers.sms import router as sms_router
from routers.statistics import router as statistics_router
from routers.auth import router as auth_router
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

app.include_router(clients_router)
app.include_router(sendings_router)
app.include_router(messages_router)
app.include_router(sms_router)
app.include_router(statistics_router)
app.include_router(auth_router)


@app.get("/", include_in_schema=False)
def home(request: Request):
    return templates.TemplateResponse("home.html", context={"request": request})


# @app.on_event("startup")
# def startup():
#     print("Create tables...")
#     Base.metadata.create_all()
