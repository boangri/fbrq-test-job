from pydantic_settings import BaseSettings

class Settings(BaseSettings):
    RABBITMQ_USER: str = 'admin'
    RABBITMQ_PASS: str = 'mypass'
    RABBITMQ_HOST: str = 'rabbit'
    RABBITMQ_PORT: int = 5672
    POSTGRES_USER: str = 'myuser'
    POSTGRES_PASS: str = 'password'
    POSTGRES_HOST: str = 'db'
    POSTGRES_PORT: int = 5432
    POSTGRES_DB: str   = 'data'


settings = Settings()
