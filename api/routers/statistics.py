from fastapi import APIRouter, status
from schemas import Stat
import models
from database import db

router = APIRouter(prefix="/api")


@router.get('/statistics/', tags=["Statistics"], summary="получить статистику по всем рассылкам",
            response_model=Stat, status_code=status.HTTP_200_OK)
def get_sms() -> Stat:
    stat = Stat()
    messages = db.query(models.Message).all()
    sendings = db.query(models.Sending).all()
    stat.sendings_total = len(sendings)
    for msg in messages:
        stat.sms_total += 1
        if msg.status == 0:
            stat.status_pending += 1
        elif msg.status == 1:
            stat.status_sent += 1
        elif msg.status == 2:
            stat.status_expired += 1

    return stat


@router.get('/statistic/{id}', tags=["Statistics"], summary="получить статистику по рассылке sending_id",
            response_model=Stat, status_code=status.HTTP_200_OK)
def get_sms(id: int) -> Stat:
    stat = Stat()
    messages = db.query(models.Message).filter(models.Message.sending_id == id)
    stat.sendings_total = 1
    for msg in messages:
        stat.sms_total += 1
        if msg.status == 0:
            stat.status_pending += 1
        elif msg.status == 1:
            stat.status_sent += 1
        elif msg.status == 2:
            stat.status_expired +=1

    return stat
