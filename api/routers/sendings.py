from fastapi import APIRouter, status, HTTPException, Depends
from typing import List
from schemas import Sending
import models
from database import db
from worker import create_task
from datetime import datetime
from auth.jwt_bearer import jwtBearer

router = APIRouter(prefix="/api")


# @router.get('/sendings', response_model=List[Sending], status_code=status.HTTP_200_OK)
@router.get('/sendings', tags=["Sendings"], summary="получить список всех рассылок", status_code=status.HTTP_200_OK)
def get_all_sendings() -> List[Sending]:
    sendings = db.query(models.Sending).all()
    return {'sendings': sendings}


@router.options('/sendings', include_in_schema=False, tags=["Sendings"], status_code=status.HTTP_200_OK)
def get_options():
    pass


@router.get('/sending/{id}', tags=["Sendings"], summary="получить атрибуты рассылки по ее id", response_model=Sending,
            status_code=status.HTTP_200_OK)
def get_sending(id: int) -> Sending:
    sending = db.query(models.Sending).filter(models.Sending.id == id).first()
    if sending is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Sending id={id} not found')
    return sending


@router.post('/sendings', dependencies=[Depends(jwtBearer())], tags=["Sendings"],
             summary="создать новую рассылку", status_code=status.HTTP_201_CREATED)
def create_sending(sending: Sending) -> Sending:
    """ Создать новую рассылку:
        - создаем запись в БД;
        - запускаем задание celery.
    """
    sending = models.Sending(**sending.dict())
    db.add(sending)
    db.commit()
    db.refresh(sending)
    sending_id = sending.id

    if sending.start_ts > datetime.now():
        delta = sending.start_ts - datetime.now()
        countdown = int((delta.total_seconds()))
        print(f"Countdown: {countdown} secs.")
        task = create_task.apply_async(args=(sending.id, sending.code, sending.tag), countdown=countdown)
    else:
        task = create_task.delay(sending.id, code=sending.code, tag=sending.tag)

    print(task, 'id=', sending_id)
    return sending


@router.put('/sending/{id}', tags=["Sendings"], summary="обновить атрибуты рассылки", status_code=status.HTTP_200_OK)
def update_sending(id: int, sending: Sending) -> Sending:
    _sending = db.query(models.Sending).filter(models.Sending.id == id).first()
    if _sending is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Sending id={id} not found')
    _sending.start_ts = sending.start_ts
    _sending.stop_ts = sending.stop_ts
    _sending.code = sending.code
    _sending.tag = sending.tag

    db.commit()
    db.refresh(_sending)
    return _sending


@router.delete('/sending/{id}', tags=["Sendings"], summary="удалить рассылку", response_model=Sending,
               status_code=status.HTTP_200_OK)
def delete_sending(id: int) -> Sending:
    sending = db.query(models.Sending).filter(models.Sending.id == id).first()
    if sending is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Sending id={id} not found')
    db.delete(sending)
    db.commit()
    return sending
