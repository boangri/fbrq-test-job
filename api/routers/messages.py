from fastapi import APIRouter, status, HTTPException
from typing import List
from schemas import Message
import models
from database import db

router = APIRouter(prefix="/api")


# @router.get('/messages', response_model=List[Message], status_code=status.HTTP_200_OK)
@router.get('/messages', tags=["Messages"], summary="получить список всех сообщений", status_code=status.HTTP_200_OK)
def get_all_messages() -> List[Message]:
    messages = db.query(models.Message).all()
    return {'messages': messages}


@router.get('/message/{id}', tags=["Messages"], summary="получить атрибуты сообщения по его id", response_model=Message, status_code=status.HTTP_200_OK)
def get_message(id: int) -> Message:
    message = db.query(models.Message).filter(models.Message.id == id).first()
    if message is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} not found')
    return message


@router.post('/messages', tags=["Messages"], summary="создать новое сообщение", status_code=status.HTTP_201_CREATED)
def create_message(message: Message) -> Message:
    message = models.Message(**message.dict())
    db.add(message)
    db.commit()
    db.refresh(message)
    return message


@router.put('/message/{id}', tags=["Messages"], summary="обновить атрибуты сообщения", status_code=status.HTTP_200_OK)
def update_message(id: int, message: Message) -> Message:
    _message = db.query(models.Message).filter(models.Message.id == id).first()
    if _message is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} not found')
    _message.sent_ts = message.sent_ts
    _message.status = message.status
    _message.sending_id = message.sending_id
    _message.client_id = message.client_id

    db.commit()
    db.refresh(_message)
    return _message


@router.delete('/message/{id}', tags=["Messages"], summary="удалить сообщение", response_model=Message, status_code=status.HTTP_200_OK)
def delete_message(id: int) -> Message:
    message = db.query(models.Message).filter(models.Message.id == id).first()
    if message is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} not found')
    db.delete(message)
    db.commit()
    return message
