from fastapi import APIRouter, status, HTTPException
from typing import List
from schemas import Client
import models
from database import db

router = APIRouter(prefix="/api")

# @router.get('/clients', response_model=List[Client], status_code=status.HTTP_200_OK)
@router.get('/clients', tags=["Clients"], summary="получить список всех клиентов", status_code=status.HTTP_200_OK)
def get_all_clients() -> List[Client]:
    clients = db.query(models.Client).all()
    return {'clients': clients}


@router.get('/client/{id}', tags=["Clients"], summary="получить клиента по его id", response_model=Client, status_code=status.HTTP_200_OK)
def get_client(id: int) -> Client:
    client = db.query(models.Client).filter(models.Client.id == id).first()
    if client is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Client id={id} not found')
    return client


@router.post('/clients', tags=["Clients"], summary="добавить нового клиента в справочник со всеми его атрибутами", status_code=status.HTTP_201_CREATED)
def create_client(client: Client) -> Client:
    # try:
    client = models.Client(**client.dict())
    # except PhoneFormatError as err:
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
    #                         detail=err)
    db_client = db.query(models.Client).filter(models.Client.phone == client.phone).first()
    if db_client is not None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=f'Client with phone={client.phone} already exists')
    db.add(client)
    db.commit()
    db.refresh(client)
    return client


@router.put('/client/{id}', tags=["Clients"], summary="обновить атрибуты клиента с данным id", status_code=status.HTTP_200_OK)
def update_client(id: int, client: Client) -> Client:
    _client = db.query(models.Client).filter(models.Client.id == id).first()
    if _client is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Client id={id} not found')
    _client.phone = client.phone
    _client.code = client.code
    _client.tag = client.tag
    _client.tz = client.tz

    db.commit()
    db.refresh(_client)
    return _client


@router.delete('/client/{id}', tags=["Clients"], summary="удалить клиента", response_model=Client, status_code=status.HTTP_200_OK)
def delete_client(id: int) -> Client:
    client = db.query(models.Client).filter(models.Client.id == id).first()
    if client is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Client id={id} not found')
    db.delete(client)
    db.commit()
    return client
