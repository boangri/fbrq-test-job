from fastapi import APIRouter, status, HTTPException
from schemas import Sms, Message
import models
from database import db
from datetime import datetime

router = APIRouter(prefix="/api")


@router.get('/sms/{id}', tags=["Sms"], summary="получить текст и номер телефона по id", response_model=Sms,
            status_code=status.HTTP_200_OK)
def get_sms(id: int) -> Sms:
    msg = db.query(models.Message).filter(models.Message.id == id).first()
    if msg is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} not found')
    if msg.status != 0:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} has been already sent')
    sending_id = msg.sending_id
    client_id = msg.client_id
    client = db.query(models.Client).filter(models.Client.id == client_id).first()
    sending = db.query(models.Sending).filter(models.Sending.id == sending_id).first()
    if client is None or sending is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Attributes for message id={id} not found')
    text = sending.text
    phone = client.phone

    """ тут надо добавить проверку что не истек срок отправки """
    stop_ts = sending.stop_ts
    if stop_ts:
        # ts = datetime.strptime(stop_ts[:19], '%Y-%m-%dT%H:%M:%S')
        if datetime.now() > stop_ts:
            msg.status = 2 # means expired
            db.commit()
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail=f'Message id={id} has expired')
    sms = Sms(phone=phone, text=text)
    return sms


@router.put('/sms/{id}', tags=["Sms"], summary="обновить статус сообщения", status_code=status.HTTP_200_OK)
def update_sms(id: int) -> Message:
    _msg = db.query(models.Message).filter(models.Message.id == id).first()
    if _msg is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'Message id={id} not found')
    _msg.status = 1
    _msg.sent_ts = datetime.now()
    db.commit()
    db.refresh(_msg)
    return _msg
