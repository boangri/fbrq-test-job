.PHONY: up
up: docker-compose.yml
	docker compose -f docker-compose.yml up -d

.PHONY: down
down: docker-compose.yml
	docker compose -f docker-compose.yml down